console.log('Loading event');
var AWS = require('aws-sdk');
//var dynamodb = new AWS.DynamoDB();
var https = require('https');
var querystring = require('querystring');
var TelegramBot = require('node-telegram-bot-api');

var botAPIKey = '311726349:AAGIA4aKU7fC4WHTvSuXSA9ZKShRZC3x0kk';

var STICKER = false;
var STICKER_PATH = "";

exports.handler = function(event, context) {
    // Log the basics, just in case
    console.log("Request received:\n", JSON.stringify(event));
    console.log("Context received:\n", JSON.stringify(context));

    var bot = new TelegramBot(botAPIKey);

    bot.onText(/\/echo (.+)/, function (msg, match) {
      // 'msg' is the received Message from Telegram
      // 'match' is the result of executing the regexp above on the text content
      // of the message

      var chatId = msg.chat.id;
      var resp = match[1]; // the captured "whatever"

      // send back the matched "whatever" to the chat
      bot.sendMessage(chatId, resp);
    });

    var com = event.message.text.toLowerCase();
    var text = "";
    switch(com){
        case "\/start":
            text = "<strong>Bienvenido al bot de Universidad Ultra</strong>";
            STICKER = true;
            STICKER_PATH = 'stickers/happy.webp';
            break;
        case "\/description":
            text = "<strong>Bot para Universidad Ultra</strong>\n<em>Mándame un mensaje</em>";
            break;
        case "\/sup":
            text = "¿Qué hay de nuevo, "+event.message.from.first_name+"?";
            STICKER = true;
            STICKER_PATH = 'stickers/sup.webp';
            break;
        case "\/star":
            text = "¿No habrás querido decir /start?";
            break;

        //En caso de ser el comando de noticias
        case "\/noticias":
            text = "<strong>Últimas noticias:</strong>\n\n";
            text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/334-conoce-las-fotografias-ganadoras'>· Conoce las fotografías Ganadoras / Promesas de México</a>\n";
            text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/332-nombramiento-enrique-guasarrama'>· Nombramiento - Enrique Guadarrama</a>\n";
            text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/331-nombramiento-luis-cabrera'>· Nombramiento- Luis Cabrera</a>\n";
            break;

        //En caso de que el mensaje sea saludo
        case "hola":
        case "hello":
        case "hi":
        case "holi":
        case "holiwis":
        case "holiwis kiwis":
          text = "Hola, "+event.message.from.first_name;
          STICKER = true;
          STICKER_PATH = 'stickers/sup.webp';
          break;

        default:
            text = "Hola, <em>"+event.message.from.first_name+"</em>. Esta es una prueba de respuesta. Tu mensaje fue <em>\""+event.message.text+"\"</em>";
            break;
    }

    var message = event.message.text;
    var post_data;

    var options = {
      parse_mode: 'HTML'
    };

    bot.sendMessage(event.message.chat.id, text, options);

    if(STICKER){
      bot.sendSticker(event.message.chat.id, STICKER_PATH);
      STICKER = false;
    }
}
