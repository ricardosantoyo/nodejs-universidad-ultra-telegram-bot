var TelegramBot = require('node-telegram-bot-api');
var request = require('request');

var botAPIKey = '311726349:AAGIA4aKU7fC4WHTvSuXSA9ZKShRZC3x0kk';

var _admin_id = 15456563;

exports.botCommand = botCommand;
exports.botMessage = botMessage;
exports.botResponse = botResponse;

var STICKER = false;
var STICKER_PATH = "";

var BROADCAST = false;

var options = {
  parse_mode: 'HTML',
  disable_web_page_preview: true
};

/**
* Función para procesar los comandos del bot, se usaría un switch-case
* @param CMD: String. Comando a ejecutar
* @param ARGS: String Array. Argumentos del comando, null por defecto
* @param message: Object. Json del mensaje
*/
function botCommand(CMD, ARGS, message){

  var bot = new TelegramBot(botAPIKey);
  var text = "";
  var bcMsg = "";

  CMD = CMD.replace("\/", "");
  if(ARGS == null){
    //Comando único
    console.log("Comando único: "+CMD);
    /********** BEGIN SWITCH-CASE ***********/
    switch (CMD) {
      case "broadcast":
          if(message.from.id != _admin_id){
            text = "Este comando es para uso exclusivo de los administradores.";
          } else {
            text = "<strong>Mensajes Broadcast</strong>\n\nTodos los mensajes que mandes a continuación serán enviados a todos los usuarios. Para detener el envío de mensajes envía /stopbr";
            updatebroadcast(bot, message.from.id, 1);
            BROADCAST = true;
          }
          break;
      case "stopbr":
          updatebroadcast(bot, message.from.id, 0);
          text = "<strong>Envío de mensajes broadcast terminado</strong>";
          break;
      case "start":
          text = "<strong>Bienvenido al bot de Universidad Ultra.</strong>\n\nEnvia /ayuda para ver la lista de comandos que me puedes enviar.";
          STICKER = true;
          STICKER_PATH = 'stickers/greet.webp';
          verifyUser(message.from);
          break;
      case "descripcion":
          text = "<strong>Bot para Universidad Ultra.</strong>\n\nPuedes enviar /ayuda para ver la lista de comandos que me puedes enviar.";
          break;
      case "hora":
          var date = new Date();
          date.setHours(date.getHours() - 6);
          var datestring =  date.getHours() + ":" + date.getMinutes();
          text = "Aquí está la hora :)\n\nSon las <strong>"+datestring+"</strong>";
          break;
      case "buzon":
          text = "No dejaste ningún mensaje. Prueba escribiendo <em>/buzon TU_MENSAJE</em>";
          STICKER = true;
          STICKER_PATH = 'stickers/shrug.webp';
          break;
      case "sup":
          text = "¿Qué hay de nuevo, "+message.from.first_name+"?";
          STICKER = true;
          STICKER_PATH = 'stickers/greet.webp';
          break;
      case "ayuda":
          text = "<em>Comandos que puedes utilizar:\n\n</em>";
          text += "/ayuda - Comandos que se pueden utilizar\n";
          text += "/descripcion - Descripción del bot de Universidad Ultra\n";
          text += "/noticias - Recibe las últimas noticias de Universidad Ultra\n";
          text += "\nPara dudas, quejas o sugerencias puedes dejar un mensaje con el comando <em>/buzon</em>.\n\n<strong>Ejemplo:</strong> <em>/buzon TU MENSAJE AQUÍ</em>";
          break;
      case "star":
          text = "¿No habrás querido decir /start?";
          break;

      //En caso de ser el comando de noticias
      case "noticias":
          text = "<strong>Últimas noticias:</strong>\n\n";
          text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/334-conoce-las-fotografias-ganadoras'>· Conoce las fotografías Ganadoras / Promesas de México</a>\n";
          text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/332-nombramiento-enrique-guasarrama'>· Nombramiento - Enrique Guadarrama</a>\n";
          text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/331-nombramiento-luis-cabrera'>· Nombramiento- Luis Cabrera</a>\n";
          break;

      case "responder":
          text = "<strong>Uso indebido del comando</strong>\n\nUsa el comando de la siguiente forma: <em>/responder [CHAT ID] [MENSAJE]</em>. \n\nEjemplo: <em>/responder 123456789 Hola</em>";
          break;

      case "secretos":
          text = "<em>Comandos secretos para administradores:\n\n</em>";
          text += "/responder - Responde a un mensaje de buzón de un usuario\n";
          text += "/hora - Te da la hora del servidor (- 6)\n\n";
          text += "<strong>Broadcast</strong>\n"
          text += "/broadcast + MENSAJE - Manda mensaje rápido a todos los usuarios suscritos a este bot.\n";
          text += "/broadcast - Manda varios mensajes a todos los usuarios suscritos a este bot.\n";
          text += "/stopbr - Detiene la transmisión de mensajes broadcast.\n";
          break;

      default:
          text = "Hola, "+message.from.first_name+". No pude procesar tu comando.\nPuedes consultar la lista de comandos enviando /ayuda";
          STICKER = true;
          STICKER_PATH = 'stickers/shrug.webp';
          break;
    }
    /********** END SWITCH-CASE ***********/
  } else {
    //Comando con argumentos
    console.log("Comando con argumentos: "+CMD);
    CMD = CMD.replace("\/", "");
    switch (CMD) {
      case "broadcast":
        if(message.from.id == _admin_id){
          text = "<strong>Mensaje Broadcast</strong>\n\nTu mensaje se está enviando a todos los suscriptores de este bot...";
          bot.sendMessage(message.chat.id, text, options);
          bcMsg = message.text.replace("\/broadcast ", "");
          broadcast(bot, bcMsg, message.chat.id);
          return;
        } else {
          text = "Este comando es para uso exclusivo de los administradores.";
        }
        break;
      case "buzon":
        text = "Mensaje del usuario "+message.from.first_name+" "+message.from.last_name;
        if("username" in message.from){
          text += " (<a href='http://telegram.me/"+message.from.username+"'>@"+message.from.username+"</a>)";
        }
        text += "\n\n";
        var msg = message.text.replace("\/buzon ", "");
        text += "<strong>Mensaje:</strong> <em>"+msg+"</em>\n\n";
        text += "<strong>Para responder, sólo responde a este mensaje con tu respuesta.</strong>\n\n";
        text += "- Chat ID: "+message.chat.id+"\n";
        text += "- Message ID: "+message.message_id+"\n";
        text += "- User ID: "+message.from.id+"\n";
        var date = new Date(message.date * 1000);
        date.setHours(date.getHours() - 6);
        var datestring = date.getDate()  + "\/" + (date.getMonth()+1) + "\/" + date.getFullYear() + " a las " + date.getHours() + ":" + date.getMinutes();
        text += "- Fecha: "+datestring;
        //Enviar mensaje al administrador
        bot.sendMessage(_admin_id, text, options);
        //text = "<em>/responder "+message.chat.id+" AQUI_ESCRIBE_TU_MENSAJE</em>";
        //bot.sendMessage(_admin_id, text, options);

        text = "Tu mensaje fue enviado al administrador satisfactoriamente. Gracias por tus comentarios.";
        STICKER = true;
        STICKER_PATH = 'stickers/thumbs_up.webp';
        break;

      case "responder":
        var id_response = parseInt(ARGS[0]);
        if(!isNaN(id_response)){
          if(ARGS.length < 2){
            text = "<strong>No proporcionaste un mensaje.</strong> \n\nUsa el comando de la siguiente forma: <em>/responder [CHAT ID] [MENSAJE]</em>. \n\nEjemplo: <em>/responder 123456789 Hola</em>";
          } else {
            text = "<strong>Respuesta a tu mensaje: </strong>\n\n";
            var msg = message.text.replace("\/responder ", "");
            msg = msg.replace(ARGS[0] + " ", "");
            text += msg;
            bot.sendMessage(id_response, text, options);

            text = "<strong>Respuesta enviada al ID "+id_response+"</strong>";
          }
        } else {
          text = "<strong>ID del chat no válido.</strong> \n\nUsa el comando de la siguiente forma: <em>/responder [CHAT ID] [MENSAJE]</em>. Ejemplo: <em>/responder 123456789 Hola</em>";
        }
        break;

      default:
        text = "Enviaste un comando con argumentos no reconocidos.\nEnvia /ayuda para obtener información de los comandos.";
        STICKER = true;
        STICKER_PATH = 'stickers/shrug.webp';
    }

    for(var i = 0; i < ARGS.length; i++){
      console.log("Argumento "+i+": "+ARGS[i]);
    }
  }

  bot.sendMessage(message.chat.id, text, options);

  if(STICKER){
    bot.sendSticker(message.chat.id, STICKER_PATH);
    STICKER = false;
  }
}

/**
* Función para procesar los mensajes
* TODO: Implementar IA
* @param MSG: String. Mensaje a procesar.
* @param message: Object. Json del mensaje
*/
function botMessage(MSG, message){

  var bot = new TelegramBot(botAPIKey);
  var text = "";

  console.log("Mensaje: "+MSG);

  if(message.from.id == _admin_id){
    checkbroadcast(bot, MSG, message.from.id, message);
    if(BROADCAST){
      return;
    }
  }

  switch(MSG){
    case "hola":
    case "hello":
    case "hi":
    case "holi":
    case "holiwis":
    case "holo":
    case "holiwis kiwis":
      text = "Hola, "+message.from.first_name;
      STICKER = true;
      STICKER_PATH = 'stickers/greet.webp';
      break;
    case "hate":
    case "hate you":
    case "te odio":
    case "me caes mal":
    case "i hate you":
    case "me caes gordo":
    case "no te soporto":
      text = ":(";
      break;
    case "love you":
    case "i love you":
    case "i heart you":
    case "i <3 you":
    case "i<3u":
    case "i <3 u":
    case "te amo":
    case "te quiero":
    case "te adoro":
      text = "Aprecio el detalle, "+message.from.first_name+". Pero el amor es algo que no puedo procesar.";
      break;
    case ":)":
      text = ":)";
      break;
    case "sexo":
    case "tengamos sexo":
    case "hagamos el amor":
    case "fuck me":
    case "cójeme":
    case "cojeme":
    case "te cojo":
    case "sex":
    case "let\'s have sex":
    case "have sex":
    case "bésame":
    case "besame":
    case "te beso":
    case "dame un beso":
    case "kiss me":
    case "kiss":
      text = "😳";
      break;
    case "que onda":
    case "qué onda":
    case "qué onda?":
    case "que onda?":
    case "¿qué onda?":
    case "¿que onda?":
        text = "¿Qué onda, "+message.from.first_name+"?";
        STICKER = true;
        STICKER_PATH = 'stickers/greet.webp';
        break;
    case "quién eres":
    case "quien eres":
    case "quien eres?":
    case "quién eres?":
    case "¿quién eres?":
    case "¿quien eres?":
    case "qué eres":
    case "que eres":
    case "que eres?":
    case "qué eres?":
    case "¿qué eres?":
    case "¿que eres?":
    case "como te llamas":
    case "cómo te llamas":
    case "como te llamas?":
    case "cómo te llamas?":
    case "¿como te llamas?":
    case "¿cómo te llamas?":
        text = "Hola, "+message.from.first_name+". Me llamo <strong>Fipu</strong>, un Bot para Universidad Ultra, y mi trabajo es mejorar tu experiencia con la plataforma. \n\nPuedes consultar lo que puedo hacer escribiendo /ayuda.";
        STICKER = true;
        STICKER_PATH = 'stickers/greet.webp';
        break;

    case "rola el pack papu :v":
    case "rola el pack":
    case "pasa el pack papu :v":
    case "pasa el pack papu":
    case "pasa el pack":
    case "pasa el pack :v":
    case "pasa el zelda":
    case "pasa el zelda papu":
    case "pasa el zelda papu :v":
        text = "Khé :v";
        break;
    case "papu":
    case ":v":
        text = ":v";
        break;

    default:
        if(!BROADCAST){
          text = "Hola, "+message.from.first_name+". No pude procesar tu mensaje.\n Tu mensaje fue <em>\""+message.text+"\"</em>";
          STICKER = true;
          STICKER_PATH = 'stickers/shrug.webp';
        } else {
          text = null;
        }
        break;
  }

  if(text != null)
    bot.sendMessage(message.chat.id, text, options);

  if(STICKER){
    bot.sendSticker(message.chat.id, STICKER_PATH);
    STICKER = false;
  }
}

/**
* Función para responder a un mensaje
* @param MSG: String. Mensaje para respuesta.
* @param message: Object. Json del mensaje
*/
function botResponse(MSG, message){
  var bot = new TelegramBot(botAPIKey);

  var n = message.reply_to_message.text.indexOf("Chat ID: ");
  if(n == -1){
    botMessage(MSG, message);
    return;
  }
  var m = message.reply_to_message.text.indexOf("Message ID: ") + 12;
  //var reply_id = message.reply_to_message.text.substring(m+12, m+15);
  var reply_id = "", aux = message.reply_to_message.text.substring(m, m+1), i = 0;
  while (aux != "\n"){
    reply_id += aux;
    i++;
    aux = message.reply_to_message.text.substring(m+i, m+i+1);
  }
  console.log("Reply ID: "+reply_id);
  var chat_id = message.reply_to_message.text.substring(n+9, n+18);
  console.log("Chat ID: "+chat_id);
  var text = "<strong>Respuesta a tu mensaje: </strong>\n\n";
  text += MSG;

  var opt = {
    parse_mode: 'HTML',
    reply_to_message_id: reply_id
  };

  bot.sendMessage(chat_id, text, opt);
  bot.sendMessage(message.chat.id, "<strong>Respuesta enviada satisfactoriamente.</strong>", options);
}

/**
* Función para verificar un usuario en la BD, si no existe lo inserta
* @param user: Object. Json del usuario
*/
function verifyUser(user){
  // Set the headers
  var headers = {
      'User-Agent':       'Super Agent/0.0.1',
      'Content-Type':     'application/x-www-form-urlencoded'
  }

  if ("username" in user) {
    var args =
    {
      'method' : 'check',
      'id': user.id,
      'first_name': user.first_name,
      'last_name' : user.last_name,
      'chat_id' : user.id,
      'username' : user.username
    }
  } else {
    var args =
    {
      'method' : 'check',
      'id': user.id,
      'first_name': user.first_name,
      'last_name' : user.last_name,
      'chat_id' : user.id,
      'username' : 'NULL'
    }
  }

  // Configure the request
  var options = {
      url: 'http://telemetrika.com.mx/univultra/telegram_bot/user_data.php',
      method: 'POST',
      headers: headers,
      form: args
  }

  // Start the request
  request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
          // Print out the response body
          var json = JSON.parse(body);

          if(json.status == "ok"){
            console.log(body)
          } else {
            console.log("Error at retrieving json");
          }
      }
  })
}

/**
* Función para verificar un usuario en la BD, si no existe lo inserta
* TODO: HACER FUNCIÓN
* @param bot: TelegramBot. Bot para hacer los envíos de mensajes
* @param msg: String. Mensaje a ser enviado
* @param origin_id: Int. ID de origen del mensaje
*/
function broadcast(bot, msg, origin_id, message){
  // Set the headers
  var headers = {
      'User-Agent':       'Super Agent/0.0.1',
      'Content-Type':     'application/x-www-form-urlencoded'
  }

  var args = { 'method' : 'broadcast' }

  // Configure the request
  var options = {
      url: 'http://telemetrika.com.mx/univultra/telegram_bot/user_data.php',
      method: 'POST',
      headers: headers,
      form: args
  }

  // Start the request
  request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
          // Parse body into json
          var json = JSON.parse(body);

          if(json.status == "ok"){
            if(json.results.length > 0){
              json.results.forEach(function (user){
                setTimeout(function() {

                  if(user.id != origin_id){
                    if("photo" in message){
                      if("caption" in message.photo){
                        bot.sendPhoto(user.id, message.photo[message.photo.length-1].file_id, message.caption);
                      } else {
                        bot.sendPhoto(user.id, message.photo[message.photo.length-1].file_id);
                      }
                    } else if("video" in message){
                      bot.sendVideo(user.id, message.video.file_id);
                    } else if("voice" in message){
                      bot.sendVoice(user.id, message.voice.file_id);
                    } else if("document" in message){
                      bot.sendDocument(user.id, message.document.file_id);
                    } else if("sticker" in message){
                      bot.sendSticker(user.id, message.sticker.file_id);
                    } else {
                      bot.sendMessage(user.id, msg, options);
                    }
                  }

                }, 35);
              });
              console.log("Broadcast sent to "+json.results.length+" users");
            }
          } else {
            console.log("Error at retrieving json");
            bot.sendMessage(origin_id, "<strong>Error:</strong> No se pudieron recuperar los usuarios. Favor de contactar al administrador.", options);
          }
      } else {
        bot.sendMessage(origin_id, "<strong>Error:</strong> No se pudo mandar el mensaje", options);
      }
  });
}

function updatebroadcast(bot, id, status){
  // Set the headers
  var headers = {
      'User-Agent':       'Super Agent/0.0.1',
      'Content-Type':     'application/x-www-form-urlencoded'
  }

  var args = {
    'method' : 'updatebroadcast',
    'id' : id,
    'status' : status
  }

  // Configure the request
  var options = {
      url: 'http://telemetrika.com.mx/univultra/telegram_bot/user_data.php',
      method: 'POST',
      headers: headers,
      form: args
  }

  // Start the request
  request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
          // Parse body into json
          console.log(body);
          var json = JSON.parse(body);

          if(json.status != "ok"){
            console.log("Error at retrieving json");
            bot.sendMessage(origin_id, "<strong>Error:</strong> No se pudo actualizar el estado del broadcast. Favor de contactar al administrador.", options);
          }
      } else {
        bot.sendMessage(origin_id, "<strong>Error:</strong> No se pudo mandar el mensaje", options);
      }
  });
}

function checkbroadcast(bot, msg, id, message){
  // Set the headers
  var headers = {
      'User-Agent':       'Super Agent/0.0.1',
      'Content-Type':     'application/x-www-form-urlencoded'
  }

  var args = {
    'method' : 'checkbroadcast',
    'id' : id
  }

  // Configure the request
  var options = {
      url: 'http://telemetrika.com.mx/univultra/telegram_bot/user_data.php',
      method: 'POST',
      headers: headers,
      form: args
  }

  // Start the request
  request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
          // Parse body into json
          console.log(body);
          var json = JSON.parse(body);

          if(json.status == "ok"){
            if(json.result.active == true){
              broadcast(bot, msg, id, message);
              BROADCAST = true;
            } else {
              BROADCAST = false;
            }
          }
      } else {
        console.log("<strong>Error:</strong> No se pudo mandar el mensaje");
      }
  });
}
