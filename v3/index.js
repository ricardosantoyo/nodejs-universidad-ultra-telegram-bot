console.log('Loading event');
var AWS = require('aws-sdk');
//var dynamodb = new AWS.DynamoDB();
var https = require('https');
var querystring = require('querystring');
var TelegramBot = require('node-telegram-bot-api');
var Functions = require("./functions");

exports.handler = function(event, context) {

  console.log("Request received:\n", JSON.stringify(event));
  console.log("Context received:\n", JSON.stringify(context));

  if(("photo" in event.message) || ("video" in event.message) || ("voice" in event.message) || ("document" in event.message) || ("sticker" in event.message)){
    Functions.botMessage("", event.message);
    return;
  }

  var STRING_TEST = event.message.text;
  STRING_TEST = STRING_TEST.toLowerCase();

  var _AUX = STRING_TEST.split(" ");

  var _CMD, _ARGS, _MSG;

  if("reply_to_message" in event.message){
    Functions.botResponse(event.message.text, event.message);
  } else {

    if(_AUX.length > 1){
      if(_AUX[0].indexOf("\/") == 0){
        //Es un comando y tiene argumentos
        _CMD = _AUX[0];
        _ARGS = new Array(_AUX.length-1);
        for(var i = 1; i < _AUX.length; i++){
          _ARGS[i-1] = _AUX[i];
        }

        Functions.botCommand(_CMD, _ARGS, event.message);
      } else {
        //Es una cadena de texto
        _MSG = STRING_TEST;
        Functions.botMessage(_MSG, event.message);
      }
    } else {
      //Puede ser un único mensaje o un comando simple
      if(STRING_TEST.indexOf("\/") == 0){
        //Es un comando único
        _CMD = STRING_TEST;
        Functions.botCommand(_CMD, null, event.message);
      } else {
        //Es un único mensaje
        _MSG = STRING_TEST;
        Functions.botMessage(_MSG, event.message);
      }
    }

  }

}
