# Telegram Bot - Universidad Ultra #

Bot hecho con NodeJS para uso exclusivo de Universidad Ultra

### ¿Para qué sirve? ###

* Obtener las últimas noticias del portal de Universidad Ultra (*)
* Obtener el calendario de siguientes días (*)
* Mandar mensajes de feedback al administrador y que este los responda
* Mandar mensajes broadcast a todos los usuarios que hacen uso del bot
* Mandar archivos de todo tipo a estos usuarios
* Puedes tener una conversación simple con el bot

(*) Pendiente

### ¿Qué comandos puedo utilizar? ###

* /start - Inicia el uso con el bot y guarda el usuario en la base de datos
* /descripcion - Regresa la descripción del bot
* /sup - Saludar a Fipu
* /noticias - Regresa las últimas noticias del portal (hardcode)
* /hora - devuelve la hora y fecha local

### Comandos para uso entre administradores y usuarios ###

* /broadcast [MENSAJE] - Envío de mensaje broadcast rápido a todos los usuarios [A]
* /broadcast - Todos los mensajes siguientes a este comando se enviarán a todos los usuarios, incluyendo todo tipo de archivos [A]
* /stopbr - Termina el broadcast de mensajes [A]
* /buzon [MENSAJE] - Manda mensaje al administrador. Este último sólo tiene que dar en responder al mensaje y se regresará la respuesta al usuario [U]
* /responder [ID CHAT] [MENSAJE] - Responde a mensaje de los usuarios (deprecado) [A]
* /secretos - Regresa todos los comandos que no están dados de alta en el BotFather [A/U]

A - Uso exclusivo del administrador
U - Uso exclusivo del usuario

### Créditos ###

* Telemetrika S.A. de C.V.
* Ing. Ricardo Santoyo Reyes