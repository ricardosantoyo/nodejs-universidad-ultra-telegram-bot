/*********** Funciones de uso general para el Bot ***********/
var TelegramBot = require('node-telegram-bot-api');

var botAPIKey = '311726349:AAGIA4aKU7fC4WHTvSuXSA9ZKShRZC3x0kk';

exports.botCommand = botCommand;
exports.botMessage = botMessage;

var STICKER = false;
var STICKER_PATH = "";

var options = {
  parse_mode: 'HTML'
};

/**
* Función para procesar los comandos del bot, se usaría un switch-case
* @param CMD: String. Comando a ejecutar
* @param ARGS = null: String Array. Argumentos del comando, null por defecto
* @param message: Object. Json del mensaje
*/
function botCommand(CMD, ARGS, message){

  var bot = new TelegramBot(botAPIKey);
  var text = "";

  CMD = CMD.replace("\/", "");
  if(ARGS == null){
    //Comando único
    console.log("Comando único: "+CMD);
    /********** BEGIN SWITCH-CASE ***********/
    switch (CMD) {
      case "start":
          text = "<strong>Bienvenido al bot de Universidad Ultra.</strong>\n\nEnvia /ayuda para ver la lista de comandos que me puedes enviar.";
          STICKER = true;
          STICKER_PATH = 'stickers/happy.webp';
          break;
      case "descripcion":
          text = "<strong>Bot para Universidad Ultra.</strong>\n\nPuedes enviar /ayuda para ver la lista de comandos que me puedes enviar.";
          break;
      case "sup":
          text = "¿Qué hay de nuevo, "+message.from.first_name+"?";
          STICKER = true;
          STICKER_PATH = 'stickers/sup.webp';
          break;
      case "star":
          text = "¿No habrás querido decir /start?";
          break;

      //En caso de ser el comando de noticias
      case "noticias":
          text = "<strong>Últimas noticias:</strong>\n\n";
          text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/334-conoce-las-fotografias-ganadoras'>· Conoce las fotografías Ganadoras / Promesas de México</a>\n";
          text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/332-nombramiento-enrique-guasarrama'>· Nombramiento - Enrique Guadarrama</a>\n";
          text += "<a href='http://universidadultra.com.mx/index.php/component/k2/item/331-nombramiento-luis-cabrera'>· Nombramiento- Luis Cabrera</a>\n";
          break;

      default:
          text = "Hola, "+message.from.first_name+". No pude procesar tu comando.\nPuedes consultar la lista de comandos enviando /ayuda";
          STICKER = true;
          STICKER_PATH = 'stickers/shrug.webp';
          break;
    }
    /********** END SWITCH-CASE ***********/
  } else {
    //Comando con argumentos
    console.log("Comando con argumentos: "+CMD);
    text = "Enviaste un comando con argumentos no reconocidos.\nEnvia /ayuda para obtener información de los comandos.";
    STICKER = true;
    STICKER_PATH = 'stickers/shrug.webp';
    for(var i = 0; i < ARGS.length; i++){
      console.log("Argumento "+i+": "+ARGS[i]);
    }
  }

  bot.sendMessage(message.chat.id, text, options);

  if(STICKER){
    bot.sendSticker(message.chat.id, STICKER_PATH);
    STICKER = false;
  }
}

/**
* Función para procesar los mensajes
* TODO: Implementar IA
* @param MSG: String. Mensaje a procesar.
* @param message: Object. Json del mensaje
*/
function botMessage(MSG, message){

  var bot = new TelegramBot(botAPIKey);
  var text = "";

  console.log("Mensaje: "+MSG);

  var greetings_array = ["hola", "hello", "hi", "holi", "holiwis", "holo", "holiwis kiwis"];

  if(greetings_array.indexOf(MSG) > -1){
    text = "Hola, "+message.from.first_name;
    STICKER = true;
    STICKER_PATH = 'stickers/sup.webp';
    break;
  }

  switch(MSG){
    case "que onda":
    case "qué onda":
    case "qué onda?":
    case "que onda?":
    case "¿qué onda?":
    case "¿que onda?":
        text = "¿Qué onda, "+message.from.first_name+"?";
        STICKER = true;
        STICKER_PATH = 'stickers/sup.webp';
        break;

    default:
        text = "Hola, "+message.from.first_name+". No pude procesar tu mensaje.\n Tu mensaje fue <em>\""+message.text+"\"</em>";
        STICKER = true;
        STICKER_PATH = 'stickers/shrug.webp';
        break;
  }

  bot.sendMessage(message.chat.id, text, options);

  if(STICKER){
    bot.sendSticker(message.chat.id, STICKER_PATH);
    STICKER = false;
  }
}
