console.log('Loading event');
var AWS = require('aws-sdk');
//var dynamodb = new AWS.DynamoDB();
var https = require('https');
var querystring = require('querystring');

var botAPIKey = '311726349:AAGIA4aKU7fC4WHTvSuXSA9ZKShRZC3x0kk';

exports.handler = function(event, context) {
    // Log the basics, just in case
    console.log("Request received:\n", JSON.stringify(event));
    console.log("Context received:\n", JSON.stringify(context));

    var text = "";
    switch(event.message.text){
        case "\/description":
            text = "Bot para Universidad Ultra\nMándame un mensaje";
            break;
        default:
            text = "Hola, "+event.message.from.first_name+". Tu mensaje fue \""+event.message.text+"\"";
            break;
    }

    var message = event.message.text;
    var post_data;

    if(message.indexOf("\/start") > -1){
        text = "Bienvenido al bot de Universidad Ultra";

        post_data = querystring.stringify({
        	'chat_id': event.message.chat.id,
        	'text': text
        });

    } else {
        post_data = querystring.stringify({
        	'chat_id': event.message.chat.id,
        	'reply_to_message_id': event.message.message_id,
        	'text': text
        });
    }

    /*
    var options = {
      hostname: 'api.telegram.org',
      port: 443,
      path: '/bot'+botAPIKey+'/sendMessage?'+post_data,
      method: 'GET'
    };

    var req = https.request(options, (res) => {
      console.log('statusCode:', res.statusCode);
      console.log('headers:', res.headers);

      res.on('data', (d) => {
        process.stdout.write(d);
      });
    });

    req.on('error', (e) => {
      console.error(e);
    });
    req.end();*/

    //POST
    // Build the post string from an object

  // An object of options to indicate where to post to
  var post_options = {
      hostname: 'api.telegram.org',
      port: '443',
      path: '/bot'+botAPIKey+'/sendMessage',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': post_data.length
      }
  };

  // Set up the request
  var post_req = https.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();



    // Now into the logging portion (DynamoDB)
    /*console.log("Into the dynamo");

    // You can change this variable if you want.
    var tableName = "telegramlog";
    var datetime = new Date().getTime().toString();

    // Put all the data into Dynamo
    dynamodb.putItem({
            "TableName": tableName,
            "Item": {
                "username": {
                    "S": event.message.from.username
                },
                "updateid": {
                    "N": event.update_id.toString()
                },
                "datetime": {
                    "N": datetime
                },
                "fullmessage": {
                    "S": JSON.stringify(event)
                }
            }
        }, function(err, data) {
            if (err) {
                context.fail('ERROR: Dynamo failed: ' + err);
            } else {
                console.log('Dynamo Success: ' + JSON.stringify(data, null, '  '));
            }
        });
        */
}
